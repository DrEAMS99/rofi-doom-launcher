# Rofi DOOM Launcher

This launcher allows you to use doom source ports with seperate sets of mods defined in different folders. there is a default config you can edit in the base script for options like fast monsters and default difficulty level just in case your source port doesn't have that option.

Each folder within the $doom2moddirectory or $doommoddirectory can hold seperate wad files that will be loaded together when you select that folder within the mods option. An easy example would be when selecting the btsxe1 folder it will load both the btsx_e1a.wad and btsx_e1b.wad but not the btsx_e1.txt.

As of now the launcher only supports .wad files but I'm willing to try to get it to work with .pk3s and .dehs (I think loading wads in most source ports already loads the deh with them but I could be wrong).

I've only tested this with prboom-plus so its definitly possible that it might not work perfectly with other source ports yet.

EDIT VERSION 1.00

After 9 Months i actually added TNT and Plutonia support and many other features.

These Include

setting directories for plutonia and tnt mods

advanced options setting

a "save" feature that saves which map and the complevel used on specific wads

##TO DO

find a way to add advanced options throught the launcher to be used in saves
